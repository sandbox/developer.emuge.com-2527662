<?php

$sql = "
INSERT INTO emugecom_state_info (state_id, state_abbr, state_name, territory_number) VALUES
(51, 'AB', 'Alberta', 93),
(2, 'AK', 'Alaska ', 101),
(1, 'AL', 'Alabama ', 80),
(4, 'AR', 'Arkansas ', 263),
(3, 'AZ', 'Arizona ', 34),
(52, 'BC', 'British Columbia', 93),
(5, 'CA', 'California ', 34),
(6, 'CO', 'Colorado ', 50),
(7, 'CT', 'Connecticut ', 1),
(8, 'DE', 'Delaware ', 25),
(9, 'FL', 'Florida ', 90),
(10, 'GA', 'Georgia ', 80),
(11, 'HI', 'Hawaii ', 101),
(15, 'IA', 'Iowa ', 56),
(12, 'ID', 'Idaho ', 40),
(13, 'IL', 'Illinois', 55),
(14, 'IN', ' Indiana ', 35),
(16, 'KS', 'Kansas ', 24),
(17, 'KY', 'Kentucky ', 36),
(18, 'LA', 'Louisiana ', 263),
(21, 'MA', 'Massachusetts ', 1),
(53, 'MB', 'Manitoba', 93),
(20, 'MD', 'Maryland ', 25),
(19, 'ME', 'Maine ', 1),
(22, 'MI', 'Michigan ', 46),
(23, 'MN', 'Minnesota ', 31),
(25, 'MO', 'Missouri ', 24),
(24, 'MS', 'Mississippi ', 263),
(26, 'MT', 'Montana ', 40),
(999, 'NA', 'National', 93),
(54, 'NB', 'New Brunswick', 93),
(33, 'NC', 'North Carolina ', 32),
(34, 'ND', 'North Dakota ', 24),
(27, 'NE', 'Nebraska', 24),
(29, 'NH', 'New Hampshire ', 1),
(30, 'NJ', 'New Jersey ', 25),
(55, 'NL', 'Newfoundland', 93),
(31, 'NM', 'New Mexico ', 261),
(56, 'NS', 'Nova Scotia', 93),
(57, 'NT', 'Northwest Territories', 93),
(58, 'NU', 'Nunavut', 93),
(28, 'NV', 'Nevada ', 28),
(32, 'NY', 'New York ', 2),
(35, 'OH', 'Ohio ', 36),
(36, 'OK', 'Oklahoma ', 262),
(59, 'ON', 'Ontario', 931),
(37, 'OR', 'Oregon ', 40),
(38, 'PA', 'Pennsylvania', 251),
(60, 'PE', 'Prince Edward Island', 93),
(100, 'EC', 'House Account', 98),
(61, 'QC', 'Quebec', 93),
(39, 'RI', 'Rhode Island ', 1),
(40, 'SC', 'South Carolina ', 32),
(41, 'SD', 'South Dakota ', 24),
(62, 'SK', 'Saskatchewan', 93),
(42, 'TN', 'Tennessee ', 32),
(43, 'TX', 'Texas ', 26),
(44, 'UT', 'Utah ', 50),
(46, 'VA', 'Virginia ', 32),
(45, 'VT', 'Vermont ', 1),
(47, 'WA', 'Washington ', 40),
(49, 'WI', 'Wisconsin ', 31),
(48, 'WV', 'West Virginia ', 36),
(50, 'WY', 'Wyoming', 50),
(63, 'YT', 'Yukon', 93)";