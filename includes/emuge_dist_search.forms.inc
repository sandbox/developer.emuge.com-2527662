<?php

function emuge_dist_search_page_form($form, &$form_state) {
    
    pc($form_state, '$form_state emuge_dist_search_page_form');
    
    foreach($form_state['build_info']['args'] as $key => $val) {
        if($key == 0) {
            $type = $val;
        } else {
            $id = $val;
        }
    }
    
    if(!isset($type)) $type = 'search';
    
    switch ($type) {
        case 'search' :
            $form['state_select'] = array(
                '#type' => 'select',
                '#title' => t('Emuge Distributor information by location'),
                '#options' => _dist_search_table_query('s'),
                '#description' => t('Pick a US state or Canadian province.'),
                '#required' => true,
                '#ajax' => array(
                    'callback' => '_ajax_update_search_page',
                    'wrapper' => 'disply-dist-results-div',
                    'method' => 'replace',
                    'effect' => 'fade',
                ),        
            );
            
        $form['results'] = array(
            '#prefix' => '<div id="disply-dist-results-div">',
            '#suffix' => '</div>',
        );
            break;
        
        case 'state' : 
            $id = check_plain($id);
            $markup = _dist_search_table_query('s', $id);
            $form['state'] = array(
                '#markup' => $markup['markup'],
            );
            
            $form['dsitributor_list'] = array(
                '#type' => 'select',
                '#options' => _dist_search_table_query('d'),
            );
            
            break;
        
        
    }
    
    return $form;

}

function _dist_search_table_query($alias, $select = 'all') {
    
    $tables = array(
        'd' => 'emugecom_distributor_info',
        's' => 'emugecom_state_info',
        'm' => 'emugecom_distributor_state_map',
    );
    
    $search_table = $tables[$alias];
    
    if($select == 'all') {
        $options = array();
        switch ($alias) {
            case 's' :
            $results = db_query("SELECT * FROM $search_table ORDER BY state_id")->fetchAll();
            foreach($results as $row) {
                $sid = $row->state_id;
                if($sid <= 50) {
                    $options[$row->state_id] = $row->state_name;
                } else if($sid < 99) {
                    $options[$row->state_id] = $row->state_name . ' Canada';
                }
            }
                break;
            case 'd' :
            $results = db_query("SELECT $alias.*, s.state_abbr FROM $search_table $alias
            INNER JOIN emugecom_state_info s ON $alias.state_id = s.state_id
            ORDER BY state_id")->fetchAll();
            foreach($results as $row) {
                $options[$row->distributor_id] = $row->distributor_name . ' ' . $row->state_abbr;
            }
            default :
        } 
    } else {
        $id = $select;
        switch ($alias) {
            case 's' :
                $results = db_query("SELECT d.* , s.state_abbr, s.state_name, m.map_id, 
                m.state_id AS map_state, m.map_weight AS map_weight 
                FROM emugecom_distributor_state_map m
                RIGHT JOIN emugecom_distributor_info d ON m.distributor_id = d.distributor_id
                INNER JOIN emugecom_state_info s ON s.state_id = d.state_id
                WHERE m.state_id = :sid ORDER BY m.map_weight", array(
                    ':sid' => $id
                ))->fetchAll();
                pc($results, '$results');
                foreach($results as $row) { 
                    $markup = !(isset($markup)) ? '<h1>Edit ' . $row->state_name . '</h1>Distributor listings in ' . $row->state_name : NULL ;
                    if($row->map_state == $row->state_id) {
                        $rows['local'][$row->distributor_id] = _add_row_search_page($row);
                    } else {
                        $rows['regional'][$row->distributor_id] = _add_row_search_page($row); 
                    }
                }
                if(isset($rows)) {
                    foreach($rows as $table_type => $row) {
                        $markup .= '<h1>' . ucfirst($table_type) . ' Distributors</h1>';
                        $header = array('Emuge Distributor', 'Phone', 'Online');
                        $markup .= theme('table', array('header' => $header, 'rows' => $row));
                    }   
                } else {
                    $markup = 'There are no local or regional distributors listed is this state.';   
                }
                
                $options = array(
                    'markup' => $markup
                );
                break;
        }
    }
        return $options;
} 


function _ajax_update_search_page($form, $form_state) {

    // get the state id and sql for processing local and regional
    $state_id = $form_state['values']['state_select'];
    $rows = array();
    
    $sql = "SELECT d.* , s.state_abbr, m.map_id, 
    m.state_id AS map_state, m.map_weight AS map_weight 
    FROM emugecom_distributor_state_map m
    INNER JOIN emugecom_distributor_info d ON d.distributor_id = m.distributor_id
    RIGHT JOIN emugecom_state_info s ON d.state_id = s.state_id
    WHERE m.state_id = :sid OR m.state_id = 99 ORDER BY weight";
    // execute sql and ready results for theme(table)
    
    $results = db_query($sql, array(':sid' => $state_id))->fetchAll();
    
     foreach($results as $row) { // process results
        
        if($row->map_state == 99 && $state_id <= 50) {
            $dist_type = 'national'; 
        } else if($state_id != $row->state_id && $row->map_state != 99) {
            $dist_type = 'regional'; 
        } else if($state_id == $row->state_id) {
            $dist_type = 'local'; 
        } else {
            $dist_type = NULL;
        }
        
        if(isset($dist_type)) {    
            $rows[$dist_type][$row->distributor_id] = _add_row_search_page($row);
            if($state_id != 47) unset($rows['national']['297']);
        }
    }
    
    $markup = '';
    
    foreach($rows as $table_type => $row) {
        $markup .= '<h1>' . ucfirst($table_type) . ' Distributors</h1>';
        $header = array('Emuge Distributor', 'Phone', 'Online');
        $markup .= theme('table', array('header' => $header, 'rows' => $row));
    }
    
    $form['results']['#markup'] = $markup;
     
    return $form['results'];
}

function _add_row_search_page($row) {
    
    // column one Emuge Distributor
    if($row->address2 != '') $row->address1 .= '<br/>' . $row->address2;
    $dist_info = $row->distributor_name . '<br/>' . $row->address1 . '<br/>';
    $dist_info .= $row->city . ' ' . $row->state_abbr . ', ' . $row->zip;
    if(user_access('administer distributor states') && $row->map_state != 99) {
        $edit_link = '<a href="/dsearch/edit/distributor/' . $row->distributor_id . '">Edit</a></em>';
        $dist_info .= '<em><br/>Map Weight: ' . $row->map_weight . ' DiD: ' . $row->distributor_id  . '<br/>' . $edit_link;
    }
    // column two Contact Information
    $dist_phone = '';
    if($row->phone != '') $dist_phone .= 'P: ' . $row->phone . '<br/>';
    if($row->toll_free_phone != '') $dist_phone .= 'P: ' . $row->toll_free_phone . '<br/>';
    if($row->fax != '') $dist_phone .= 'F: ' . $row->fax . '<br/>';
    $dist_phone = ($dist_phone != '') ? $dist_phone : 'Contact 800-323-3013 for more information.';
    
    // column three Online
    $dist_online = '';
    if($row->website) {
        $dist_online .= '<a href="http://' . $row->website . '" title="Find Emuge product at ' . $row->distributor_name . 's website">'; 
        $dist_online .= $row->website . '</a>';
    }
    if($row->email) $dist_online .= '<br/>' . $row->email;
    $dist_online = ($dist_online != '') ? $dist_online : 'Contact info@emuge.com for more infomation.';
    
    if(user_access('administer distributor states')) {
        
    }
    
    return array($dist_info, $dist_phone, $dist_online);
    
}